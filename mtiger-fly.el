
(require 'flycheck)

(flycheck-define-checker
 mtiger
 "A Tiger syntax checker"
 :command ("sml"
           ;; Change to the path of your tiger compiler
           ;; Or better yet, make it a variable the user can set.
           "@SMLload=/home/dovs/Desktop/myImpl/part6/tigerc.x86-linux"
           source-inplace
           "/tmp/mtiger-fly-out.tmp")
 :error-patterns ((error line-start (file-name) ":" line "." column ":"
                         ;; Change this to match the error messages of
                         ;; your compiler
                         (message
                          (+? not-newline)
                          (or (: ?: ?\n (repeat 2 (: (1+ not-newline) ?\n)))
                              ?\n))))
 :modes (mtiger-mode))

(defun flycheck-mtiger-setup ()
  (interactive)
  (add-to-list 'flycheck-checkers 'mtiger))

(provide 'mtiger-fly)
