; WIP

;; Add to languages:
;; Replace with paths to these files
;; (load-file "~/.emacs.d/elisp/wip/mtiger.el")
;; (load-file "~/.emacs.d/elisp/wip/mtiger-fly.el")
;;
;; (add-to-list 'auto-mode-alist '("\\.stt$" . mtiger-mode))
;; ;; Setup flycheck after flycheck has been loaded
;; (eval-after-load 'flycheck
;;   '(flycheck-mtiger-setup))
;; ;; Load flycheck when loading mtiger-mode
;; (defun mtig-hook ()
;;   (flycheck-mode 1))
;; (add-hook 'mtiger-mode-hook 'mtig-hook)

;; Add to func:
;; (defun rr ()
;;   (interactive)
;;   (unload-feature 'mtiger-mode)
;;   (reload-dotemacs-file)
;;   (load-file "~/.emacs.d/init.el")
;;   (mtiger-mode))



;;; Code:
(defvar tiger-keywords
  '("while" "for" "to" "do" "let" "in" "end" "if" "then" "else"
    "var" "function" "type" "array" "of"))

(defvar tiger-constants
  '("nil" "break"))

(defvar tiger-builtin
  '("print" "ord" "chr" "flush" "getchar" "size" "substring"
    "concat" "not" "exit" "string" "int"))


;; (defvar tiger-tab-width 2 "Width of a tab in my tiger mode")
;; (defvar tiger-tab-stop-list nil "List of columns at which tabs should end")

(defvar tiger-font-lock-defaults
  `((
     ( ,(regexp-opt tiger-keywords 'symbols) . font-lock-keyword-face)
     ( ,(regexp-opt tiger-constants 'symbols) . font-lock-constant-face)
     ( ,(regexp-opt tiger-builtin 'symbols) . font-lock-builtin-face)
     )))

;;;###autoload
(define-derived-mode mtiger-mode fundamental-mode "My Tiger Mode"
  "My Tiger mode is a bad mode for editing tiger files"
  ;; Syntax highligthin for keywords
  (setq-local font-lock-defaults tiger-font-lock-defaults)

  ;; Tab width
  ;; (if tiger-tab-width
  ;;     (setq-local tab-width tiger-tab-width))
  ;; (setq-local tab-stop-list tiger-tab-stop-list)

  ;; Comments
  (setq-local comment-start "/*")

  (setq-local comment-end "*/")
  (setq-local comment-style 'multi-line)
  (modify-syntax-entry ?/ ". 14n" mtiger-mode-syntax-table)
  (modify-syntax-entry ?* ". 23" mtiger-mode-syntax-table)
  (setq-local parse-sexp-ignore-comments t)

  ;; Declare the underscore character '_' as being a valid part of a word.
  (modify-syntax-entry ?_ "w" mtiger-mode-syntax-table)

  ;; Make +-=<> act like operators */^
  (modify-syntax-entry ?+ "." mtiger-mode-syntax-table)
  (modify-syntax-entry ?- "." mtiger-mode-syntax-table)
  (modify-syntax-entry ?= "." mtiger-mode-syntax-table)
  (modify-syntax-entry ?< "." mtiger-mode-syntax-table)
  (modify-syntax-entry ?> "." mtiger-mode-syntax-table)

  (enable-tiger-smie)
  )



;;;;;;;;
;;; SMIE
(require 'smie)
(defconst smie-tiger-grammar
  (smie-prec2->grammar
   (smie-bnf->prec2
    '((id)
          ;; We rename ':=' into '=', because vars and functions are annoying
      (exp ("let" decs "in" exp "end")
           ("if" exp "then" exp "else" exp)
           ("if" exp "then" exp)
           (exp "+" exp)
           (exp "-" exp)
           (exp "*" exp)
           (exp "/" exp)
           (exp "^" exp)
           ;; ("-" exp)                 ; Gives warning
                                        ; Not needed as exp can be empty
           (exp "=" exp)
           (exp "<>" exp)
           (exp "<" exp)
           (exp "<=" exp)
           (exp ">" exp)
           (exp ">=" exp)
           (exp "&" exp)
           (exp "|" exp)
           ("(" ")")
           ("(" seq ")")
           ("for" id "=" exp "to" exp "do" exp)
           ("while" exp "do" exp)
      ;; (id "(" seq ")") ; id can be inserted anywhere anyway
           (id "{" "}")
           (id "{" idAssL "}")
           ("nil")
           ("break")
           (lval "=" exp)
           (lval)
      )
      (lval (id)
            (lval "." id)
            (lval "[" exp "]")
      )
      (idAssL (id "=" id)
              (idAssL "," idAssL))
      (seq (exp)
           (seq "," seq)
           (seq ";" seq))
      (decs (vardec)
            (tydec)
            (fundec)
      )
      (vardec ("var" id "=" exp)
              ("var" id ":" id "=" exp))
      (tydec ("type" id "=" ty))
      (fundec ("function" p-tyfi "=" exp)
              ("function" p-tyfi ":" id "=" exp))
      (p-tyfi ("(" tyfi ")"))
      (ty (id "{" tyfi "}")             ; (non-important) id is inserted
                                        ; to prevent a warning
          ("array" "of" id)
          (id))
      (tyfi (id ":" id)
            (tyfi "," tyfi))
    )

    '((assoc ":" "="))
    '((assoc "," ";"))
    '(
      (nonassoc "for" "do" "to")
      (assoc "else" "then")
      ;; (assoc ":=")
      (assoc "&" "|")
      (assoc "=" "<>" "<" "<=" ">" ">=")
      (assoc "+" "-")
      (assoc "*" "/")
      (assoc "^")
    )

)))


(defvar smie-tiger-key-rx
  (regexp-opt '("+" "-" "*" "/" "^" "," ";" ":" "."
                ">" ">=" "<" "<=" ":=" "=" "{" "}" "(" ")" "[" "]")))

(defun smie-tiger-forward-token ()
  (forward-comment (point-max))
  (cond
   ((looking-at smie-tiger-key-rx)
    (goto-char (match-end 0))
    ;; Our grammar requires us to merge ':=' and '='
    (replace-regexp-in-string ":=" "="
         (match-string-no-properties 0)))
   ((looking-at "\"")
    (buffer-substring-no-properties
     (point)
     (progn (goto-char (+ (point) 1))   ; skip seen "
            (re-search-forward "\"")
            (point))))
   (t (buffer-substring-no-properties
       (point)
       (progn (skip-syntax-forward "w_")
              (point))))))

(defun smie-tiger-backward-token ()
  (forward-comment (- (point)))
  (cond
   ((looking-back smie-tiger-key-rx (- (point) 2) t)
    (goto-char (match-beginning 0))
    (replace-regexp-in-string
     ":=" "="
     (match-string-no-properties 0)))
   ((looking-back "\"" (- (point) 2) t)
    (buffer-substring-no-properties
     (point)
     (progn (goto-char (- (point) 1))   ; skip seen "
            (re-search-backward "\"")
            (point))))
   (t (buffer-substring-no-properties
       (point)
       (progn (skip-syntax-backward "w_")
              (point))))))

(defvar dec-prefix
  (regexp-opt '("let" "type" "function" "var") 'word))

;; Basic (fallback) indentation, nil for default value
(defvar smie-tiger-indent-basic 2)

(defun smie-tiger-rules (kind token)
  (pcase (cons kind token)
    (`(:elem . basic) smie-tiger-indent-basic)
    (`(,_ . ",") (smie-rule-separator kind))
    (`(,_ . ";") (smie-rule-separator kind))
    ;; (`(:after . ":=") smie-tiger-indent-basic)
    (`(:after . "then") smie-tiger-indent-basic)
    (`(:list-intro . "let") (smie-rule-next-p "in"))
    (`(:before . ,(or `"type" `"var" `"function"))
       (cond
             ((smie-rule-parent-p "let")
              (progn ;; (message "parent was let")
                     (smie-rule-parent smie-tiger-indent-basic)))
             (t
              (let ((pdec (prev-named-token dec-prefix)))
                (if (cddr pdec)
                    (progn ;; (message (cadr pdec))
                           `(column . ,(car pdec)))
                  (nil))))))


    (`(:before . "in")
       (if (smie-rule-parent-p "let")
             (progn ;; (message "pw let")
                    (smie-rule-parent))
           nil))
    (`(,_ . ,_) nil)))


(defun prev-named-token (tokens)
  (let ((init-point (point))
        (aux (lambda ()
               (let ((tok (smie-tiger-backward-token)))
                 (cond ((and (string-match tokens tok)
                             (= 0 (string-match tokens tok)))
                        `(,(current-column) ,tok . t))
                       ((eq "" tok)     ; At beginning of file
                        '(0 "" . nil))
                       (t
                        (progn ;; (message "aux")
                               ;; (message tok)
                               (funcall aux))))))))
    (let ((res (funcall aux)))
      (progn (goto-char init-point)
             res))))

(defun db ()
  (interactive)
  (prev-named-token dec-prefix))

(defun enable-tiger-smie ()
       (smie-setup smie-tiger-grammar
                   #'smie-tiger-rules
        :forward-token #'smie-tiger-forward-token
        :backward-token #'smie-tiger-backward-token))

(provide 'mtiger-mode)
;;; mtiger.el ends here
